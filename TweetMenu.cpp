/*
Written by Nathan Brinda
cs 101-002
project4
10/25/2015
*/

#include "TweetMenu.h"

//prompts the user given a string
std::string prompt_user(std::string text_for_prompt)
{
   std::string user_string;
   std::cout << text_for_prompt;
   std::cin.clear();
   if (std::cin.peek() == '\n')
      std::cin.ignore(INT_MAX, '\n');
   std::getline(std::cin, user_string);
   return user_string;
}

//constructor
TweetMenu::TweetMenu(HashTable *table)
{
   filled_hash_table = table;
}

//displays the menu
bool TweetMenu::process_menu()
{
   int input;
   std::cout << "1.  Enter new Tweet" << std::endl;
   std::cout << "2.  Delete all tweets for a given screen name" << std::endl;
   std::cout << "3.  View Tweets" << std::endl;
   std::cout << "4.  Quit" << std::endl;
   input = valid_input();
   
   switch (input)
   {
   case 1:
      option1();
     break;    
   case 2:
      option2();
      break;
       case 3:
      option3();
      break;
   default:
      return false;
   }

   return true;
}

//continues to prompt the user until it recieves valid input
int TweetMenu::valid_input()
{
   std::string user_choice;
   while (true)
   {
      std::cout << "Please enter an option: ";
      std::cin >> user_choice;
      
      if (user_choice != "1" && user_choice != "2" && user_choice != "3" && user_choice != "4")
         std::cout << "That is not a valid selection" << std::endl;
      else
         return std::stoi(user_choice);
   }
}

//takes in a tweet and the author of that tweet and adds it in the hash table
void TweetMenu::option1()
{
   std::string new_text;
   std::string user_names;


   new_text = prompt_user("Please enter a tweet: ");
   //std::cout << "Please enter a tweet: ";
   //std::getline(std::cin,new_text);


   user_names = prompt_user("Please enter the author of the tweet: ");
   
   //std::cout << "Please enter the author of the tweet: ";
   //std::getline(std::cin, user_names);

   time_t rawtime;
   struct tm *timeinfo;
   char buffer[80];

   time(&rawtime);
   timeinfo = localtime(&rawtime);

   strftime(buffer, 80, "%Y-%m-%dT%H:%M:%S", timeinfo);
   std::string my_time = buffer;

   Tweet *new_tweet = new Tweet(user_names, my_time, new_text);
   filled_hash_table->Put(new_tweet);
   std::cout << "Added 1 record for " << user_names << std::endl;
   std::cout << std::endl;
}

//deletes all tweets created by a given screen name
void TweetMenu::option2()
{
   std::string user_name;
   std::cout << "Please enter a screen name, all tweets by that screen name wll be deleted: ";
   std::cin >> user_name;


   int num_of_removed = filled_hash_table->Remove(user_name);

   if (num_of_removed == 0)
      std::cout << "User " << user_name << " not found" << std::endl;
   else
      std::cout << "Removed " << num_of_removed << " record(s) for user " << user_name << std::endl;

   std::cout << std::endl;

   
}

//displays all tweets created by a given screen name
void TweetMenu::option3()
{
   int counter = 0;

   std::string user_name;
   std::cout << "Please enter a screen name, all tweets by that screen name wll be displayed: ";
   std::cin >> user_name;

   Tweet *tweets_to_be_displayed = filled_hash_table->Get(user_name);

   while (tweets_to_be_displayed)
   {
      counter++;
      Tweet *next = tweets_to_be_displayed->get_next_node();
      std::cout << tweets_to_be_displayed->get_text() << std::endl;
      tweets_to_be_displayed = next;
   }

   if (counter == 0)
      std::cout << "User " << user_name << " not found" << std::endl;
   std::cout << std::endl;
}
/*
Written by Nathan Brinda
cs 101-002
project4
10/25/2015
*/

#ifndef TWEETREADER_H
#define TWEETREADER_H

#include <iostream>
#include <string>
#include <fstream>
#include "HashTable.h"
#include "Tweet.h"

class TweetReader
{

public:
   void ReadFile(char *, HashTable *table);
   TweetReader();

};

#endif
/*
Written by Nathan Brinda
cs 101-002
project4
10/25/2015
*/

#include "HashTable.h"
#include "Tweet.h"

#include <string>
#include <iomanip>
#include <ctime>
#include <sstream>
#include <iostream>
#include <climits>


class TweetMenu
{
private:
   int valid_input();
   void option1();
   void option2();
   void option3();
   HashTable *filled_hash_table;

public:
   TweetMenu(HashTable*);
   bool process_menu();
};
/*
Written by Nathan Brinda
cs 101-002
project4
10/25/2015
*/

#ifndef TWEET_H
#define TWEET_H

#include <iostream>

class Tweet
{
private:
   std::string user_name;
   std::string date_created;
   std::string text;
   Tweet* next_node;

public:
   Tweet();
   Tweet(std::string, std::string, std::string);
   std::string get_user_name();
   std::string get_data_created();
   std::string get_text();
   Tweet* get_next_node();
   void set_next_node(Tweet* new_node);
};

#endif


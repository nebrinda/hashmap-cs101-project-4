/*
Written by Nathan Brinda
cs 101-002
project4
10/25/2015
*/

#include <iostream>
#include "HashTable.h"
#include "Tweet.h"
#include "TweetReader.h"
#include "TweetMenu.h"



int main(int argc, char** argv)
{
   if (argc != 2)
   {
      std::cerr << "must supply exactly 1 command line argument";
      exit(1);
   }

   HashTable *table = new HashTable();

   TweetReader* file_reader  = new TweetReader();
   file_reader->ReadFile(argv[1], table);
   TweetMenu *menu = new TweetMenu(table);

   while (menu->process_menu())
      continue;  
   
   return 0;
}
/*
Written by Nathan Brinda
cs 101-002
project4
10/25/2015
*/

#include "Tweet.h"

Tweet::Tweet()
{}

//takes the given information and assigns private members the values
Tweet::Tweet(std::string name, std::string date, std::string info)
{
   user_name = name;
   date_created = date;
   text = info;
   next_node = NULL;
}

//returns the name of the author of the tweet
std::string Tweet::get_user_name()
{
   return user_name;
}

//returns the date the tweet was created
std::string Tweet::get_data_created()
{     
   return date_created;
}

//returns the text of the tweet
std::string Tweet::get_text()
{
   return text;
}

//returns the next node
Tweet* Tweet::get_next_node()
{
   return next_node;
}

//sets the next node
void Tweet::set_next_node(Tweet* new_node)
{
   next_node = new_node;
}




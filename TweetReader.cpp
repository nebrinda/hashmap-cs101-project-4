/*
Written by Nathan Brinda
cs 101-002
project4
10/25/2015
*/

#include "TweetReader.h"

//deletes anything that does not fall in the standard ascii table
void remove_unicode(std::string &str) //may not work
{
   int length = str.length();
   for (int i = 0; i <= length; i++)
   {
      if ((int)str[i] < 0 || (int)str[i] >= 127)
      {
         str.erase(i, i + 1);
         length = str.length();
         i--;
      }
   }
}

//gets the correct substring out of a given string
std::string extract_substring(std::string find_string, std::string line_text)
{
   int pos;
   int pos1;

   remove_unicode(line_text);

   pos = line_text.find(':');
   pos1 = line_text.find_last_of('"');
   return line_text.substr(pos + 2, pos1 - (pos + 2));
}

TweetReader::TweetReader()
{}

//reads in each tweet from then file, creates a Tweet object, and passes that object to HashTable
void TweetReader::ReadFile(char *filename, HashTable *table)
{
   std::fstream file_Pointer;
   std::string line_text;
   std::string date;
   std::string text;
   std::string author;
   int length;

   file_Pointer.open(filename);

   while (std::getline(file_Pointer, line_text)) 
   {
      length = line_text.length();

      if (length <= 1)
         continue;

      date = extract_substring("created_at", line_text);
      std::getline(file_Pointer, line_text);

      text = extract_substring("text", line_text);
      std::getline(file_Pointer, line_text);

      author = extract_substring("screen_name", line_text);

      Tweet *x = new Tweet(author, date, text);
      table->Put(x);
   }
}






/*
Written by Nathan Brinda
cs 101-002
project4
10/25/2015
*/

#include "HashTable.h"

//hash function djb2 as shown in class
unsigned long hash(unsigned char *str)
{
   unsigned long hash = 5381;
   int c;

   while ((c = *str++))
      hash = ((hash << 5) + hash) + c;

   return hash;
}

//creates the hashtable and sets all values == to NULL
HashTable::HashTable()
{
   total_elements = 0;
   number_of_keys = 0;
   size = 1033;//a decently large prime number, it is not over 7000 that way my rehash function is used
   Tweet_array = new Tweet*[size];

   for (int i = 0; i < size; i++)
   {
      Tweet_array[i] = NULL;
   }
}

//rehashes all the values into a bigger table
void HashTable::rehash()
{
   Tweet* temp_node = new Tweet();
   Tweet **temp_array = Tweet_array;

   size = size * 2;
   temp_array = new Tweet*[size];

   for (int i = 0; i < size; i++)
      temp_array[i] = NULL;

   for (int i = 0; i < (size / 2); i++)
   {
      if (Tweet_array[i] != NULL)
      {
         temp_node = Tweet_array[i];
         int index = get_hash_index(temp_node->get_user_name());
         if (temp_array[index] != NULL)
         {
            index = get_open_index(index);
            if (index < 0)
            {
               //should never occur, as the doubled table should hold everything
               std::cout << "Unable to insert record " << temp_node->get_user_name() << std::endl;
               exit(1);
            }
         }
         temp_array[index] = Tweet_array[i];
      }
   }
   delete Tweet_array;
   Tweet_array = temp_array;
}

//hashes a given value and returns its proper index
int HashTable::get_hash_index(std::string key_string)
{
   unsigned long hash_value = hash((unsigned char*)key_string.c_str());
   return hash_value % size;
}

//either inserts an item into the hash table, or adds it to the linked list
void HashTable::Put(Tweet *TW)
{
   int index = find_index_for_add(TW->get_user_name());

   //the list is empty
   if (Tweet_array[index] == NULL)
   {
      add_new_object(index, TW);
      return;
   }
   
   total_elements++;
   Tweet* previous_node = NULL;
   Tweet *current_node = Tweet_array[index];

   while (true)
   {
      if (current_node == NULL)
      {
         previous_node->set_next_node(TW);
         return;
      }

      if (TW->get_data_created() <= current_node->get_data_created())
      {
         //previous node is at the top
         if (previous_node == NULL)
         {
            Tweet_array[index] = TW;
            TW->set_next_node(current_node);
         }

         else
         {
            previous_node->set_next_node(TW);
            TW->set_next_node(current_node);
         }
        
         return;
      }
      previous_node = current_node;
      current_node = current_node->get_next_node();
   }
}

//returns the index for a given key
Tweet* HashTable::Get(std::string key_string)
{
   int index = find_index(key_string);
   
   if (index < 0)
      return NULL;

   return Tweet_array[index];
}

//returns true if the key is in the hashtable, false otherwise
bool HashTable::Contains(std::string key_string)
{
   int index = find_index(key_string);

   if (index < 0)
      return NULL;

   return Tweet_array[index] != NULL;
}

//returns total number of elements added
int HashTable::Count()
{
   return total_elements;
}

//removes all the values associated with a certain key
int HashTable::Remove(std::string key_string)
{
   int counter = 0;
   int index = find_index(key_string);

   if (index < 0)
      return 0;

   Tweet *current = Tweet_array[index];
   Tweet_array[index] = NULL;
   while (current)
   {
      counter++;
      Tweet *next = current->get_next_node();
      delete current;
      current = next;
   }

   return counter;
}

//finds the correct index for retrieval (accounts for collisions)
int HashTable::find_index(std::string key)
{
   int index = get_hash_index(key);

   //hash index is empty?
   if (Tweet_array[index] == NULL)
      return -1;

   //we used this index before?
   if (Tweet_array[index]->get_user_name() == key)
      return index;

   //we had a collision, find the new index
   for (int i = index + 1; i < size; i++)
   {
      if (Tweet_array[i]->get_user_name() == key)
         return i;
   }

   for (int i = 0; i < index; i++)
   {
      if (Tweet_array[i]->get_user_name() == key)
         return i;
   }

   return -1;
}

//finds the correct index for adding new tweets (accounts for collisions)
int HashTable::find_index_for_add(std::string key)
{
   int index = get_hash_index(key);

   //hash index is empty?
   if (Tweet_array[index] == NULL)
      return index;

   //we used this index before?
   if (Tweet_array[index]->get_user_name() == key)
      return index;

   //we have a collision, find the new index
   for (int i = index + 1; i < size; i++)
   {
      if (Tweet_array[i] == NULL)
         return i;

      if (Tweet_array[i]->get_user_name() == key)
         return i;
   }

   for (int i = 0; i < index; i++)
   {
      if (Tweet_array[i] == NULL)
         return i;

      if (Tweet_array[i]->get_user_name() == key)
         return i;
   }

   return -1;
}

//accounts for collisions during rehash
int HashTable::get_open_index(int current_index)
{
   for (int i = current_index; i < size; i++)
   {
      if (Tweet_array[i] == NULL)
         return i;
   }

   for (int i = 0; i < current_index - 1; i++)
   {
      if (Tweet_array[i] == NULL)
         return i;
   }

   return -1;
}

//adds a new tweet to the hashtable
void HashTable::add_new_object(int index, Tweet* TW)
{
   number_of_keys++;
   total_elements++;
   Tweet_array[index] = TW;

   //checking the load factor of the hash table
   if (number_of_keys >= (.6*size))
      rehash();

}


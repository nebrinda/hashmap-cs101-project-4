/*
Written by Nathan Brinda
cs 101-002
project4
10/25/2015
*/

#ifndef HASHTABLE_H
#define HASHTABLE_H

#include <iostream>
#include <string>
#include "Tweet.h"

class HashTable
{
private:
   int size;
   int total_elements;
   int number_of_keys;
   Tweet** Tweet_array;
   int get_hash_index(std::string);
   void rehash();
   int get_open_index(int);
   void add_new_object(int,Tweet*);
   int find_index_for_add(std::string);
   int find_index(std::string);

public:
   HashTable();
   void Put(Tweet*);
   Tweet* Get(std::string);
   bool Contains(std::string);
   int Count();
   int Remove(std::string);  
};

#endif